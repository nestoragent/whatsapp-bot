package com.whatsapp;

import com.whatsapp.lib.Init;
import com.whatsapp.lib.Props;

/**
 * Created by sbt-velichko-aa on 19.04.2017.
 */
public class Before {

    public static void before() {
        //login
        if (null != System.getProperty("login") && !"".equals(System.getProperty("login")))
            Init.getStash().put("login", System.getProperty("login"));
        else
            Init.getStash().put("login", Props.get("user.name"));

        //password
        if (null != System.getProperty("password") && !"".equals(System.getProperty("password")))
            Init.getStash().put("password", System.getProperty("password"));
        else
            Init.getStash().put("password", Props.get("user.password"));

        //scroll count
        if (null != System.getProperty("scrollCount") && !"".equals(System.getProperty("scrollCount")))
            Init.getStash().put("scrollCount", System.getProperty("scrollCount"));
        else
            Init.getStash().put("scrollCount", Props.get("applications.scroll.count"));
    }
}

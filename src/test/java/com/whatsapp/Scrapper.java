package com.whatsapp;

import com.whatsapp.lib.Init;
import com.whatsapp.lib.Props;
import com.whatsapp.lib.pageFactory.PageInitializationException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalTime;
import java.util.ArrayList;

/**
 * Created by nestor on 11.04.2017.
 */
public class Scrapper {

    private WebDriver driver;
    private volatile ArrayList<String> creativeMarketDesigners = new ArrayList<>();

    @BeforeClass
    public void before(){
        Before.before();
    }

    @Test()
    public void scrapper() throws PageInitializationException, InterruptedException {
        driver = Init.createWebDriver();
        driver.get(Props.get("applications.url") + "/designers");
        ArrayList<Thread> threads = new ArrayList<>();
        LocalProducer producer = new LocalProducer(null);
        for (int i = 0; i < 5; i++) {
            Thread thread = new Thread(producer);
            threads.add(thread);
            thread.start();
        }

        LocalTime timeout = LocalTime.now().plusMinutes(5);
        for (Thread thread : threads) {
            while (thread.isAlive() && LocalTime.now().isBefore(timeout))
                Thread.sleep(5 * 1000);
        }

        System.out.println("result size for creative market: " + creativeMarketDesigners.size());
//        creativeMarketDesigners.forEach(link -> {
//            User designer = new User();
//            designer.setCreativeMarket("true").setLink(link).setStatus(DesignerStatusEnum.New.toString());
//            try {
//                if (null == Factory.getInstance().getDesignersDAO().getDesignerByLink(link))
//                    Factory.getInstance().getDesignersDAO().addDesigner(designer);
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//        });
    }

    @AfterClass
    public void dispose() {
        Init.dispose(driver);
    }

    class LocalProducer implements Runnable {

        private int counter;
        private ArrayList<String> designersProfile;

        public LocalProducer(ArrayList<String> designersProfile) {
            this.designersProfile = designersProfile;
        }

        @Override
        public void run() {
            while (counter < designersProfile.size()) {
                String link;
                synchronized (this) {
                    link = designersProfile.get(counter++);
                }
            }
        }

    }
}

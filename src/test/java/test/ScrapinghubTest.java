package test;

import org.apache.commons.codec.binary.Base64;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by nestor on 31.07.2017.
 */
public class ScrapinghubTest {

    @Test
    public void makeRequest() {

        Connection con = Jsoup.connect("https://storage.scrapinghub.com/items/219027/2/7/0?apikey=ea65652819e14cc185689a062751d46a")
                .method(Connection.Method.GET)
//                .header("Authorization", "Basic " + base64login)
//                .header("X-Auth-Code", "ea65652819e14cc185689a062751d46a")
//                .("Authorization", "ea65652819e14cc185689a062751d46a")
                .header("Content-Type", "application/json")
//                .header("Authorization", "ea65652819e14cc185689a062751d46a") // these are base64 encoded
//                .requestBody(data)
                .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                .ignoreContentType(true);
        try {
            Connection.Response body = con.execute();
            System.out.println("con: " + body.statusCode());
            System.out.println("out: " + body.body());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

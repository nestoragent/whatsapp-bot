package test;

import com.whatsapp.lib.Init;
import com.whatsapp.pages.google.ContactsPage;
import com.whatsapp.pages.google.InputEmailPage;
import com.whatsapp.pages.google.InputPasswordPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

/**
 * Created by sbt-velichko-aa on 11.05.17.
 */
public class TestGoogle {

    private WebDriver driver;

    @Test
    public void testLink() throws Exception {
        driver = Init.createWebDriver();
        driver.get("https://contacts.google.com/");

        new InputEmailPage(driver).inputEmail("sao.paulo.vila.mariana@gmail.com");

        new InputPasswordPage(driver).inputPassword("TestPassword1");

//
//        ApproveLoginPage approveLoginPage = new ApproveLoginPage();
//        approveLoginPage.press_button(approveLoginPage.buttonByEmail);
//        approveLoginPage.inputApproveEmail("");

        ContactsPage contactsPage = new ContactsPage(driver);
        contactsPage.addNewContact("test3", "8646148");
    }

    @AfterClass
    public void dispose() {
        Init.dispose(driver);
    }

}


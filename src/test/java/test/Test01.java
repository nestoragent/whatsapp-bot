package test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by sbt-velichko-aa on 11.05.17.
 */
public class Test01 {

    RemoteWebDriver threadDriver = null;

    public void setUp() throws MalformedURLException {

        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
//        chromePrefs.put("download.default_directory", downloadDir.getAbsolutePath());
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
            DesiredCapabilities cap = DesiredCapabilities.chrome();
        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        cap.setCapability(ChromeOptions.CAPABILITY, options);

        File chromeDriver;
                    chromeDriver = new File("src/test/resources/webdrivers/linux64");
        System.setProperty("webdriver.chrome.driver", chromeDriver.getAbsolutePath());
        cap.setBrowserName("chrome");
//        new ChromeDriver(cap);

        threadDriver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), cap);
    }

    public WebDriver getDriver() {
        return threadDriver;
    }

    public void closeBrowser() {
        getDriver().quit();

    }

    public void testLink()throws Exception{
        setUp();
        getDriver().get("http://facebook.com");
        WebElement textBox = getDriver().findElement(By.cssSelector("input[name='firstname']"));
        textBox.click();
        textBox.sendKeys("Just a test 01!!!!");
        closeBrowser();

//        java -Dwebdriver.chrome.driver=/home/sbt-velichko-aa/git/whatsapp-scrapper/src/test/resources/webdrivers/linux64/chromedriver -jar selenium-server-standalone-2.53.1.jar -role webdriver -hub http://localhost:4444/grid/register -port 5558 -browser browserName="chrome",version=ANY,maxInstances=3
    }

}

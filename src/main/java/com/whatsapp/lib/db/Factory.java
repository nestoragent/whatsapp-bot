package com.whatsapp.lib.db;

import com.whatsapp.DAO.PeopleDAO;
import com.whatsapp.DAO.PeopleDAOImpl;
import com.whatsapp.DAO.UserDAO;
import com.whatsapp.DAO.UserDAOImpl;

/**
 * Created by sbt-velichko-aa on 20.03.17.
 */
public class Factory {

    private static Factory instance = null;
    private static UserDAO userDAO = null;
    private static PeopleDAO peopleDAO = null;

    public static synchronized Factory getInstance() {
        if (instance == null) {
            instance = new Factory();
        }
        return instance;
    }

    public UserDAO getUserDAO() {
        if (userDAO == null) {
            userDAO = new UserDAOImpl();
        }
        return userDAO;
    }

    public PeopleDAO getPeopleDAO() {
        if (peopleDAO == null) {
            peopleDAO = new PeopleDAOImpl();
        }
        return peopleDAO;
    }
}

package com.whatsapp.lib;

import com.whatsapp.lib.pageFactory.AutotestError;
import com.whatsapp.lib.pageFactory.PageFactory;
import com.whatsapp.lib.util.OsCheck;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

//import org.sikuli.script.IRobot;
//import org.sikuli.script.Screen;

/**
 * Created by VelichkoAA on 12.01.2016.
 */
public class Init {

    private static final Map<String, Object> stash = new HashMap<>();
    private static PageFactory pageFactory;
    private static String os = Props.get("automation.os");
    private static String testInstrument = Props.get("automation.instrument");
    private static String timeout = Props.get("webdriver.page.load.timeout");
//    private static Screen screen;
//    private static IRobot robot;
//
//    public static Screen getScreen() {
//        if (null == screen)
//            screen = new Screen();
//        return screen;
//    }
//
//    public static IRobot getRobot() {
//        if (null == robot)
//            robot = getScreen().getRobot();
//        return robot;
//    }


    public static PageFactory getPageFactory() {
        if (null == pageFactory) {
            pageFactory = new PageFactory("com.whatsapp.pages");
        }
        return pageFactory;
    }

    public static WebDriver createWebDriver() {
        WebDriver driver;
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
        File downloadDir = new File("download");
        OsCheck checkOS = new OsCheck();

        if (null == testInstrument) {
            testInstrument = "Firefox";
        }

        switch (BrowserEnum.valueOf(testInstrument)) {
            case Firefox:
                capabilities.setBrowserName("firefox");
                FirefoxProfile fProfile = new FirefoxProfile();
                fProfile.setPreference(FirefoxProfile.PORT_PREFERENCE, 7054);
//                fProfile.setAcceptUntrustedCertificates(true);
//                fProfile.setPreference("browser.download.dir", downloadDir.getAbsolutePath());
//                fProfile.setPreference("browser.download.folderList", 2);
//                fProfile.setPreference("browser.download.manager.showWhenStarting", false);
//                fProfile.setPreference("browser.helperApps.alwaysAsk.force", false);
//                fProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
//                fProfile.setPreference("browser.helperApps.neverAsk.saveToDisk",
//                        "application/zip;application/octet-stream;application/x-zip;application/x-zip-compressed");
//                fProfile.setPreference("plugin.disable_full_page_plugin_for_types", "application/zip");
//
//                capabilities.setJavascriptEnabled(true);
                capabilities.setCapability(FirefoxDriver.PROFILE, fProfile);
                driver = new FirefoxDriver(capabilities);

                break;
            case Safari:
                System.setProperty("webdriver.safari.noinstall", "true");
                capabilities.setBrowserName("safari");
                driver = new SafariDriver(capabilities);
                break;
            case Chrome:
                HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
                chromePrefs.put("profile.default_content_settings.popups", 0);
                chromePrefs.put("download.default_directory", downloadDir.getAbsolutePath());
                ChromeOptions options = new ChromeOptions();
//                String userProfile = "/home/sbt-velichko-aa/.config/google-chrome/Default";
//                options.addArguments("user-data-dir=" + userProfile);
                File ext1 = new File("2.2_0.crx");
                options.addExtensions(new File(ext1.getAbsolutePath()));
                options.setExperimentalOption("prefs", chromePrefs);
                DesiredCapabilities cap = DesiredCapabilities.chrome();
                cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                cap.setCapability(ChromeOptions.CAPABILITY, options);

                File chromeDriver;
                switch (checkOS.getOperationSystemType()) {
                    case MacOS:
                        chromeDriver = new File("src/test/resources/webdrivers/mac/chromedriver");
                        break;
                    case Windows:
                        chromeDriver = new File("src/test/resources/webdrivers/windows/chromedriver.exe");
                        break;
                    case Linux:
                        if (checkOS.getArchType().equals("64"))
                            chromeDriver = new File("src/test/resources/webdrivers/linux64/chromedriver");
                        else
                            chromeDriver = new File("src/test/resources/webdrivers/linux32/chromedriver");
                        break;
                    default:
                        throw new AutotestError("Unsupported system. Os = " + System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH));
                }
                System.setProperty("webdriver.chrome.driver", chromeDriver.getAbsolutePath());
                cap.setBrowserName("chrome");
                driver = new ChromeDriver(cap);
                break;
            case PhantomJS:
                capabilities.setJavascriptEnabled(true);
                capabilities.setCapability("takesScreenshot", true);

                File phantomDriver = null;
                switch (checkOS.getOperationSystemType()) {
                    case Windows:
                        phantomDriver = new File("src/test/resources/webdrivers/windows/phantomjs.exe");
                        break;
                    case MacOS:
                        phantomDriver = new File("src/test/resources/webdrivers/mac/phantomjs");
                        break;
                    case Linux:
                        if (checkOS.getArchType().equals("64"))
                            phantomDriver = new File("src/test/resources/webdrivers/linux64/phantomjs");
                        else
                            phantomDriver = new File("src/test/resources/webdrivers/linux32/phantomjs");
                        break;
                    default:
                        throw new AutotestError("Unsupported system. Os = " + System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH));
                }
                capabilities.setCapability(
                        PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
                        phantomDriver.getAbsolutePath()
                );
                System.setProperty("phantomjs.binary.path", phantomDriver.getAbsolutePath());
                driver = new PhantomJSDriver(capabilities);
                break;
            default:
                capabilities.setBrowserName("firefox");
                driver = new FirefoxDriver(capabilities);
                break;
        }
        driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return driver;
    }

    public static void dispose(WebDriver driver) {
        if (os.equals("Android") || os.equals("IOS")) {
        } else {
            try {
                System.out.println("Checking any alert opened");
                WebDriverWait alertWait = new WebDriverWait(driver, 2);
                alertWait.until(ExpectedConditions.alertIsPresent());
                Alert alert = driver.switchTo().alert();
                System.out.println("Got and alert: " + alert.getText() + "\n closing it.");
                alert.dismiss();
            } catch (Exception | Error e) {
                System.err.println("No alert opened. Closing webdriver.");
            }
            Set<String> windowsHandlerSet = driver.getWindowHandles();
            try {
                if (windowsHandlerSet.size() > 2) {
                    final WebDriver finalDriver = driver;
                    windowsHandlerSet.forEach((winHandle) -> {
                        finalDriver.switchTo().window(winHandle);
                        ((JavascriptExecutor) finalDriver).executeScript(
                                "var objWin = window.self;" +
                                        "objWin.open('', '_self', '');'" +
                                        "objWin.close();");
                    });
                }

            } catch (Exception e) {
                System.err.println("Failed to kill all of the browser windows. Error message = " + e.getMessage());
            }
        }
        try {
            driver.quit();
        } catch (Exception | Error e) {
            System.err.println("Failed to quit web driver. Error message = " + e.getMessage());
        }
        driver = null;
        pageFactory = null;
    }

    public static DriverExt getDriverExtensions() {
        return new DriverExt();
    }


    public static int getTimeOut() {
        return Integer.parseInt(timeout);
    }

    public static int getTimeOutInSeconds() {
        return Integer.parseInt(timeout) / 1000;
    }

    public static Map<String, Object> getStash() {
        return stash;
    }

    public static byte[] takeFullScreenshot(WebDriver driver) throws AWTException, IOException {
        if (null != driver) {
            ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            java.awt.Rectangle screenBounds = new java.awt.Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            BufferedImage picture = new Robot().createScreenCapture(screenBounds);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ImageIO.write(picture, "png", bytes);
            return bytes.toByteArray();
        }
        return "".getBytes();
    }

    public static String getOs() {
        return os;
    }

    public static String getTestInstrument() {
        return testInstrument;
    }
}

package com.whatsapp.lib.util;

import com.whatsapp.lib.Init;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
//import org.sikuli.script.FindFailed;
//import org.sikuli.script.IRobot;
//import org.sikuli.script.ImagePath;
//import org.sikuli.script.Screen;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

/**
 * Created by sbt-velichko-aa on 21.07.17.
 */
@Slf4j
public class SikuliHelper {

    private static boolean screenIsExist(String screenshotPath) {
        try {
//            Init.getScreen().exists(screenshotPath, 1).isValid();
            return true;
        } catch (NullPointerException e) {
            log.debug("Window " + screenshotPath + " didn't found.", e);
            return false;
        }
    }

    public String waitAndClick(WebDriver driver) {
        new WebDriverWait(driver, Init.getTimeOutInSeconds()).until((ExpectedCondition<Boolean>) d ->
                (screenIsExist("\\src\\test\\resources\\files\\sikuli\\iedownload\\rus\\savecancelbuttons.png")
                        ||
                        screenIsExist("\\src\\test\\resources\\files\\sikuli\\iedownload\\eng\\savecancelbuttons.png")));
//        Init.getRobot().keyDown(KeyEvent.VK_CONTROL);
//        Init.getRobot().keyDown(KeyEvent.VK_C);
//        Init.getRobot().keyUp(KeyEvent.VK_C);
//        Init.getRobot().keyUp(KeyEvent.VK_CONTROL);
        try {
            return (String) Toolkit.getDefaultToolkit()
                    .getSystemClipboard().getData(DataFlavor.stringFlavor);
        } catch (UnsupportedFlavorException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void inputIndividualAccount(WebDriver driver, String key) {
        File file = new File("src/test/resources/images");
//        ImagePath.add(file.getAbsolutePath());
//        new WebDriverWait(driver, Init.getTimeOutInSeconds()).until((ExpectedCondition<Boolean>) d ->
//                (screenIsExist("accountManagerLogo.png")));
//        try {
//            Init.getScreen().click("accountManagerLogo.png");
//            Init.getScreen().wait("import.png");
//            Init.getScreen().click("import.png");
//            Init.getScreen().wait("importIndividual.png");
//            Init.getScreen().click("importIndividual.png");
//            Init.getScreen().wait("importInput.png");
//            Init.getScreen().click("importInput.png");
////            Init.getScreen().write(key);
//
//            StringSelection stringSelection = new StringSelection(key);
//            Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
//            clpbrd.setContents(stringSelection, null);
//
//            Thread.sleep(1000);
//            Init.getRobot().keyDown(KeyEvent.VK_CONTROL);
//            Init.getRobot().keyDown(KeyEvent.VK_V);
//            Init.getRobot().keyUp(KeyEvent.VK_V);
//            Init.getRobot().keyUp(KeyEvent.VK_CONTROL);
////
//            Init.getScreen().wait("importButton.png");
//            Init.getScreen().click("importButton.png");
//            Init.getScreen().wait("playButton.png");
//            Init.getScreen().click("playButton.png");
//            Init.getScreen().wait("accountManagerLogoDone.png");
//            Init.getScreen().click("accountManagerLogoDone.png");
//        } catch (FindFailed | InterruptedException findFailed) {
//            findFailed.printStackTrace();
//        }
    }
}

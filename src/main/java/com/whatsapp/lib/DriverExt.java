package com.whatsapp.lib;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Set;

/**
 * Created by sbt-velichko-aa on 29.02.2016.
 */
public class DriverExt {

    public void waitForPageToLoad(WebDriver driver, boolean... stopRecursion) {
        long timeOutTime = System.currentTimeMillis() + Init.getTimeOut();
        while (timeOutTime > System.currentTimeMillis()) {
            try {
                if ((((JavascriptExecutor) driver).executeScript("return document.readyState").toString())
                        .equals("complete"))
                    return;
            } catch (Exception | AssertionError e) {
                System.err.println("Page doesn't become to ready state. Error message = " + e.getMessage());
//                driver.navigate().refresh();
//                System.err.println("Page refreshed.");
//                if ((stopRecursion.length == 0) || (stopRecursion.length > 0 && !stopRecursion[0]))
//                    waitForPageToLoad(true);

            }
        }
    }

    public WebElement waitUntilElementAppearsInDom(WebDriver driver, By by) {
        Wait wait = new WebDriverWait(driver, Init.getTimeOutInSeconds());
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
        return driver.findElement(by);
    }

    public void waitForElementGetEnabled(WebElement element, long timeout) throws Exception {
        long deadLine = System.currentTimeMillis() + timeout;
        while (deadLine > System.currentTimeMillis()) {
            try {
                Thread.sleep(1000);
                if (element.isEnabled())
                    return;
            } catch (Exception e) {
                System.err.println("Target element still not enable. Erroe message = " + e.getMessage());
            }
        }
        throw new Exception("Timeout of waiting the element has expired. \nExpected element: " + element + ".");
    }

    public String findNewWindowHandle(WebDriver driver, Set<String> existingHandles, int milliSec) throws Exception {
        for (long timeOutTime = System.currentTimeMillis() + (long) milliSec; timeOutTime > System.currentTimeMillis();
             Thread.sleep(100L)) {
            Set<String> currentHandles = driver.getWindowHandles();
            if (currentHandles.size() != existingHandles.size() || currentHandles.size() == existingHandles.size()
                    && !currentHandles.equals(existingHandles)) {
                for (String currentHandle : currentHandles) {
                    if (!existingHandles.contains(currentHandle))
                        return currentHandle;
                }
            }
        }
        throw new Exception("No modal window found");
    }


}

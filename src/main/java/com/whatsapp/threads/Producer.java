package com.whatsapp.threads;

import com.whatsapp.entry.People;
import com.whatsapp.entry.User;
import com.whatsapp.lib.Init;
import com.whatsapp.pages.google.ApproveLoginPage;
import com.whatsapp.pages.google.ContactsPage;
import com.whatsapp.pages.google.InputEmailPage;
import com.whatsapp.pages.google.InputPasswordPage;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

/**
 * Created by sbt-velichko-aa on 25.04.17.
 */
public class Producer implements Runnable {

    private WebDriver driver;
    private ArrayList<People> peoplesList;
    private User user;

    public Producer(ArrayList<People> peoplesList, User user) {
        this.peoplesList = peoplesList;
        this.user = user;
    }

    @Override
    public void run() {
        System.out.println(" + start work thread " + Thread.currentThread().getName());
        //add contact to gmail
        System.out.println("people list: " + peoplesList.size());
        loginToGmail(user.getGmailAccount(), user.getGmailPassword());
        for (People people : peoplesList) {
             addContactToGmail(people);
        }
        disposeWebDriverProducer();

        System.out.println(" - end work thread " + Thread.currentThread().getName());
    }

    private void loginToGmail(String email, String password) {
        try {
            driver = Init.createWebDriver();
            driver.get("https://contacts.google.com/");
            new InputEmailPage(driver).inputEmail(email);
            new InputPasswordPage(driver).inputPassword(password);

//            ApproveLoginPage approveLoginPage = new ApproveLoginPage(driver);
//            approveLoginPage.press_button(approveLoginPage.buttonByEmail);
//            approveLoginPage.inputApproveEmail("nestor_007@mail.ru");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addContactToGmail(People people) {
        ContactsPage contactsPage = new ContactsPage(driver);
        contactsPage.addNewContact(people.getName(), people.getPhoneNumber());
    }

    public void disposeWebDriverProducer() {
        driver.quit();
    }

}

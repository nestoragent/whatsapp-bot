package com.whatsapp.controller;

import com.whatsapp.entry.People;
import com.whatsapp.entry.TaskData;
import com.whatsapp.entry.User;
import com.whatsapp.threads.Producer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;

/**
 * Created by nestor on 04.07.2017.
 */
@Slf4j
@Controller
@RequestMapping("/")
public class AdminController {

    private ExecuteStatus jobOneStatus = ExecuteStatus.stopped;
    private boolean execute = true;
    ArrayList<Thread> threads = new ArrayList<>();

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView welcome() {
        ModelAndView mv = new ModelAndView();
        execute = false;

        for (Thread thread : threads) {
            if (thread.isAlive()) {
                System.out.println("+ live thread: " + thread.getName());
                jobOneStatus = ExecuteStatus.working;
                execute = true;
                break;
            }
        }
        if (!execute)
            jobOneStatus = ExecuteStatus.stopped;

        mv.addObject("jobOneStatus", jobOneStatus);
        mv.addObject("taskData", new TaskData());
        mv.setViewName("index");
        log.debug("Return index page");
        return mv;
    }

    @RequestMapping(value = "jobOneStart", method = {RequestMethod.POST})
    public String jobObeStart(@ModelAttribute("taskData") TaskData taskData, BindingResult result) {
        if (result.hasErrors()) {
            System.out.println("error");
        }
        System.out.println("mes: " + taskData.getMessage());
        System.out.println("path: " + taskData.getPath());
        executeCommand();
        jobOneStatus = ExecuteStatus.working;

        log.debug("Return index page");
        return "redirect:/";
    }

    @RequestMapping(value = "jobOneExecuteControllerStop", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView jobObeStop() {
        terminateThread();
        ModelAndView mv = new ModelAndView();
        RedirectView view = new RedirectView("", true);
        view.setExposeModelAttributes(false);
        mv.setView(view);

        log.debug("Return index page");
        return mv;
    }

    private void executeCommand() {
        ArrayList<People> peoples = new ArrayList<>();
        People people = new People();
        People people2 = new People();
        people.setName("test1");
        people.setPhoneNumber("986513");
        people2.setName("test2");
        people2.setPhoneNumber("12345");

        peoples.add(people);
        peoples.add(people2);

        User user = new User();
        user.setGmailAccount("sao.paulo.vila.mariana@gmail.com");
        user.setGmailPassword("TestPassword1");
        Producer producer = new Producer(peoples, user);
//        for (int i = 0; i < 5; i++) {
            Thread thread = new Thread(producer);
            threads.add(thread);
            thread.start();
//        }
    }

    private void terminateThread() {
        System.out.println("size: " + threads.size());
        System.out.println("threads: " + threads);
        threads.stream().filter(Thread::isAlive).forEach(thread -> {
            System.out.println("- kill thread: " + thread.getName());
            thread.interrupt();
        });
    }
}

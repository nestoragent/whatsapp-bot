package com.whatsapp.controller;

/**
 * Created by sbt-velichko-aa on 21.07.17.
 */
public enum ExecuteStatus {
    stopped,
    working
}

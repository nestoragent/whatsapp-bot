package com.whatsapp.DAO;

import com.whatsapp.entry.People;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by sbt-velichko-aa on 17.03.17.
 */
public interface PeopleDAO {

    public void addPeople(People people) throws SQLException;

    public void updatePeople(People people) throws SQLException;

    public People getPeopleById(int peopleId) throws SQLException;

    public List getAllPeoples() throws SQLException;

    public void deletePeople(People people) throws SQLException;

    public People getLastPeople() throws SQLException;

    public List<People> getAllPeoplesByStatus(String status) throws SQLException;
}

package com.whatsapp.DAO;

import com.whatsapp.entry.User;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by sbt-velichko-aa on 17.03.17.
 */
public interface UserDAO {

    public void addUser(User user) throws SQLException;

    public void updateUser(User user) throws SQLException;

    public User getUserById(int userId) throws SQLException;

    public List getAllUsers() throws SQLException;

    public void deleteUser(User user) throws SQLException;

    public User getLastUser() throws SQLException;
}

package com.whatsapp.DAO;


import com.whatsapp.entry.PeopleStatusEnum;
import com.whatsapp.entry.People;
import com.whatsapp.lib.db.HibernateUtil;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sbt-velichko-aa on 17.03.17.
 */
@Slf4j
public class PeopleDAOImpl implements PeopleDAO {

    @Override
    public void addPeople(People people) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(people);
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Error at addPeople", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void updatePeople(People people) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(people);
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Error at updatePeople", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public People getPeopleById(int people_id) throws SQLException {
        People people = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            people = (People) session.load(People.class, people_id);
        } catch (Exception e) {
            log.error("Error 'findById'", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return people;
    }

    @Override
    public List getAllPeoples() throws SQLException {
        Session session = null;
        List peoples = new ArrayList<People>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            peoples = session.createCriteria(People.class).addOrder(Order.desc("id")).list();
        } catch (Exception e) {
            log.error("Error 'getAll'", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return peoples;
    }

    @Override
    public void deletePeople(People people) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(people);
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error("Error at deletePeople", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public People getLastPeople() throws SQLException {
        Session session = null;
        People people = new People();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            people = (People) session.createCriteria(People.class).addOrder(Order.desc("id")).list().get(0);
        } catch (Exception e) {
            log.error("Error 'get Last'", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return people;
    }

    @Override
    public List<People> getAllPeoplesByStatus(String status) throws SQLException {
        List<People> peoples = new ArrayList<>();
        Session session = null;
        try {
            PeopleStatusEnum curStatus = PeopleStatusEnum.valueOf(status);
            session = HibernateUtil.getSessionFactory().openSession();
            String hql = "from People where status = :status";
            Query query = session.createQuery(hql);
            query.setString("status", curStatus.toString());
            peoples = query.list();
        } catch (Exception e) {
            log.error("Error 'getAllPeoplesByStatus'", e);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return peoples;
    }
}

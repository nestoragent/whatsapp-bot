package com.whatsapp.entry;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Entity;

/**
 * Created by sbt-velichko-aa on 13.04.2017.
 */
@Entity
@Data
@Builder
@Slf4j
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private int id;
    private String gmailAccount;
    private String gmailPassword;
    private String whatsAppKey;
    private String sentTimestamp;

}

package com.whatsapp.entry;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by nestor on 02.08.2017.
 */
@Data
@Builder
@Slf4j
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TaskData {

    private String message;
    private String path;
}

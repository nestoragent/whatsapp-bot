package com.whatsapp.entry;

/**
 * Created by sbt-velichko-aa on 13.04.2017.
 */
public enum PeopleStatusEnum {
    New,
    Sent,
    Work
}

package com.whatsapp.entry;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Entity;

/**
 * Created by sbt-velichko-aa on 13.04.2017.
 */
@Entity
@Slf4j
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class People {

    @Getter @Setter
    private int id;
    @Getter @Setter
    private String phoneNumber;
    @Getter @Setter
    private String name;
    private PeopleStatusEnum status;
    @Getter @Setter
    private String sentTimestamp;


    public String getStatus() {
        return status.toString();
    }

    public void setStatus(String status) {
        this.status = PeopleStatusEnum.valueOf(status);
    }

}

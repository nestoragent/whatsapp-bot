package com.whatsapp.pages;

import com.whatsapp.lib.Init;
import com.whatsapp.lib.Page;
import com.whatsapp.lib.pageFactory.AutotestError;
import com.whatsapp.lib.pageFactory.ElementTitle;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Set;

/**
 * Created by Nestor on 18.03.2016.
 */
public class AnyPage extends Page {

    @FindBy(css = "a[href='/quickmatch']")
    @ElementTitle("quickmatch")
    public WebElement linkQuickmatch;

    public void wait_new_handler(WebDriver driver, String handlerCount) {
        try {
            int repeatCount = 0;
            while (driver.getWindowHandles().size() != Integer.parseInt(handlerCount) && repeatCount < 10) {
                sleep(1);
                repeatCount++;
            }
        } catch (Exception e) {
            System.err.println("Error when wait new handler. Message = " + e.getMessage());
        }
    }

    public void switch_to_the_new_page(WebDriver driver) throws Exception {
        String newHandler = Init.getDriverExtensions().
                findNewWindowHandle(driver, (Set<String>) Init.getStash().get("mainWindowHandle"), Init.getTimeOut());
        driver.switchTo().window(newHandler);
        driver.switchTo().defaultContent();
        driver.switchTo().activeElement();
    }

    public void close_new_page(WebDriver driver) throws Exception {
        driver.close();
        driver.switchTo().window(((Set<String>) Init.getStash().get("mainWindowHandle")).iterator().next());
        driver.switchTo().defaultContent();
        driver.switchTo().activeElement();
    }

    protected WebElement findElementByTextFromList(WebDriver driver, By list, String text) {
        Init.getDriverExtensions().waitForPageToLoad(driver);
        List<WebElement> items = driver.findElements(list);
        return items.stream().filter(el -> el.getText().equalsIgnoreCase(text)).findFirst().orElse(null);
    }

    protected WebElement findElementByTextFromList(WebDriver driver,    By list, By inner, String text) {
        Init.getDriverExtensions().waitForPageToLoad(driver);
        List<WebElement> items = driver.findElements(list);
        return items.stream().filter(el -> el.findElement(inner).getText().
                equalsIgnoreCase(text)).findFirst().orElse(null);
    }

    public void selectCheckbox(WebElement webElement) throws IllegalAccessException {
        if (!webElement.isSelected())
            press_button(webElement);
    }
}
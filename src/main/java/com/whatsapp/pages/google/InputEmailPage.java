package com.whatsapp.pages.google;

import com.whatsapp.lib.Init;
import com.whatsapp.lib.pageFactory.ElementTitle;
import com.whatsapp.pages.AnyPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by sbt-velichko-aa on 20.07.17.
 */
public class InputEmailPage extends AnyPage {

    private WebDriver driver;
    
    @FindBy(id = "identifierId")
    @ElementTitle("identifierId")
    public WebElement inputEmail;

    @FindBy(id = "identifierNext")
    @ElementTitle("identifierNext")
    public WebElement buttonNext;

    public InputEmailPage(WebDriver driver) {
        this.driver = driver;
        driver.get("https://contacts.google.com/");
        PageFactory.initElements(driver, this);
        new WebDriverWait(driver, Init.getTimeOutInSeconds()).
                until(ExpectedConditions.presenceOfElementLocated(By.id("identifierId")));
    }

    public void inputEmail(String email) {
        fill_field(inputEmail, email);
        press_button(buttonNext);
    }
}

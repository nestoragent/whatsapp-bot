package com.whatsapp.pages.google;

import com.whatsapp.lib.Init;
import com.whatsapp.lib.pageFactory.ElementTitle;
import com.whatsapp.pages.AnyPage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by sbt-velichko-aa on 20.07.17.
 */
public class ContactsPage extends AnyPage {

    @FindBy(css = "c-wiz div[role='button'] > content")
    @ElementTitle("add new contact")
    public WebElement buttonAddNewContact;
    private WebDriver driver;

    public ContactsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        new WebDriverWait(driver, Init.getTimeOutInSeconds()).
                until(ExpectedConditions.visibilityOf(buttonAddNewContact));
    }

    public void addNewContact(String name, String phone) {
        press_button(buttonAddNewContact);
        new WebDriverWait(driver, Init.getTimeOutInSeconds()).
                until(ExpectedConditions.visibilityOf(buttonAddNewContact));

        new WebDriverWait(driver, Init.getTimeOutInSeconds()).
                until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input[type='tel']")));
        WebElement inputName = driver.findElement(By.cssSelector("content[id] input[jsname][type='text']:first-child"));
        WebElement inputPhone = driver.findElement(By.cssSelector("input[type='tel']"));
        fill_field(inputName, name);
        fill_field(inputPhone, phone);

        List<WebElement> saveButton = driver.findElements(By.cssSelector("div[role='dialog'] > content[id] div[role='button'] content span[class]"));
        press_button(saveButton.get(saveButton.size() - 1));

//        Actions actions = new Actions(driver);
//        actions.keyDown(Keys.LEFT_CONTROL).sendKeys(inputName, Keys.ENTER).build().perform();
//        actions.release();

        new WebDriverWait(driver, Init.getTimeOutInSeconds()).
                until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='dialog'] > content[id] div[role='button']:last-child")));
        WebElement closeButton = driver.findElement(By.cssSelector("div[role='dialog'] > content[id] div[role='button']:last-child"));
        press_button(closeButton);
        sleep(1);
//        new WebDriverWait(driver, Init.getTimeOutInSeconds()).
//                until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[aria-label='Close']")));
//        actions.sendKeys(driver.findElement(By.cssSelector("div[aria-label='Close']")), Keys.ESCAPE).build().perform();
    }
}

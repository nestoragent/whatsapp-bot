package com.whatsapp.pages.google;

import com.whatsapp.lib.Init;
import com.whatsapp.lib.pageFactory.ElementTitle;
import com.whatsapp.pages.AnyPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by sbt-velichko-aa on 20.07.17.
 */
public class InputPasswordPage extends AnyPage {

    private WebDriver driver;
    
    @FindBy(css = "input[name='password']")
    @ElementTitle("paasword")
    public WebElement inputPassword;

    @FindBy(id = "passwordNext")
    @ElementTitle("passwordNext")
    public WebElement buttonNext;

    public InputPasswordPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        new WebDriverWait(driver, Init.getTimeOutInSeconds()).
                until(ExpectedConditions.visibilityOf(inputPassword));
    }

    public void inputPassword(String password) {
        fill_field(inputPassword, password);
        press_button(buttonNext);
    }
}

package com.whatsapp.pages.google;

import com.whatsapp.lib.Init;
import com.whatsapp.lib.pageFactory.ElementTitle;
import com.whatsapp.pages.AnyPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by sbt-velichko-aa on 20.07.17.
 */
public class ApproveLoginPage extends AnyPage {
    
    private WebDriver driver;
    
    @FindBy(css = "div[role='presentation'] ul div[data-challengetype='12']")
    @ElementTitle("By email")
    public WebElement buttonByEmail;

    @FindBy(id = "knowledge-preregistered-email-response")
    @ElementTitle("input email")
    public WebElement inputEmail;

    @FindBy(id = "next")
    @ElementTitle("next")
    public WebElement buttonNext;

    public ApproveLoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        new WebDriverWait(driver, Init.getTimeOutInSeconds()).
                until(ExpectedConditions.visibilityOf(buttonByEmail));
    }

    public void inputApproveEmail(String email) {
        new WebDriverWait(driver, Init.getTimeOutInSeconds()).
                until(ExpectedConditions.visibilityOf(inputEmail));
        fill_field(inputEmail, email);
        press_button(buttonNext);
    }

}

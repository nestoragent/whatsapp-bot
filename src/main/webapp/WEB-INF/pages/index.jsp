<%@ page session="false" %>
<mvc:annotation-driven ignoreDefaultModelOnRedirect="true"/>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid text-center">
    <div class="row content">
        <div class="col-sm-12 text-center">
            <h1>Welcome</h1>
            <p>Admin console for whatsapp bot</p>
            <hr>
        </div>
    </div>
    <div class="row content">
        <div class="col-sm-12 text-center">
            <div class="wrapper">
                <p>Job 1: check user for exist at whatsapp</p>
                <p>Status ${jobOneStatus}</p>
                <%--<form action="#" th:action="@{/jobOneStart}" th:object="${taskData}" method="post">--%>
                    <%--<p>message: <input type="text" th:field="*{message}"/></p>--%>
                    <%--<p>path: <input type="text" th:field="*{path}"/></p>--%>
                    <%--<p><input type="submit" value="Start"/> <input type="reset" value="Reset"/></p>--%>
                <%--</form>--%>

                <form:form method="POST" action="/admin/jobOneStart" modelAttribute="taskData">
                    <table>
                        <tr>
                            <td><form:label path="message">Name</form:label></td>
                            <td><form:input path="message"/></td>
                        </tr>
                        <tr>
                            <td><form:label path="path">Age</form:label></td>
                            <td><form:input path="path"/></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input class="btn btn-lg btn-primary" type="submit" value="Start"/>
                            </td>
                        </tr>
                    </table>
                </form:form>
                <%--<spring:url value="jobOneExecuteControllerStart" var="jobOneStart"/>--%>
                <%--<button class="btn btn-lg btn-primary" onclick="location.href='${jobOneStart}'">Start</button>--%>
                <spring:url value="jobOneExecuteControllerStop" var="jobOneStop"/>
                <button class="btn btn-lg btn-primary" onclick="location.href='${jobOneStop}'">Stop</button>
            </div>
        </div>
        <div class="col-sm-12 text-center">
            <div class="wrapper">

            </div>
        </div>
    </div>
</div>

</body>
</html>
